#ifndef MAPA_HPP
#define MAPA_HPP
#include <vector>
#include <utility>
#include <string>
#include "../inc/player.hpp"

using namespace std;

class Mapa{
        private:
        int tamanho;

    public: 

        Mapa();
        ~Mapa();

        int get_tamanho();
        void set_tamanho(int tamanho);

        
        void cria_mapa(char tabuleirop1[13][13], char tabuleirop2[13][13]);
        
                                //Linha     Coluna Tipo
        void le_mapa(vector <pair <int, pair <int, string>>> *vector1, vector <pair <int, pair <int, string>>> *vector2);

};

#endif