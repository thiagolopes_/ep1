#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <string>

using namespace std;

class Player{
    private:
        string nome;
        int vida;
    public:
        Player();
        ~Player();

        string get_nome();
        void set_nome(string nome);

        int get_vida();
        void set_vida(int vida);

        void perde_vida(int vida);

};

#endif