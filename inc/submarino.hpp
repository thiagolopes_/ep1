#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP
#include <utility>
#include <string>
#include "embarcacao.hpp"

using namespace std;

class Submarino: public Embarcacao{

    public:
        Submarino();
        ~Submarino();
        
        bool destroi_submarino(pair <int, int> jogada_p1, pair<int, int> jogada_p2, int turno);
};

#endif