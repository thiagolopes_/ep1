#ifndef JOGO_HPP
#define JOGO_HPP

#include <utility>
#include <vector>
#include <string>
#include <iostream>
#include "../inc/player.hpp"

using namespace std;

class Jogo{
    public:

        Jogo();
        ~Jogo();

        void recebe_jogada(pair<int, int> *jogada_p1, pair<int, int> *jogada_p2, int turno);
        void valida_jogada(vector<pair <int, pair <int, string> > > *vector1, vector<pair <int, pair <int, string> > > *vector2, char tabuleirop1[13][13], char tabuleirop2[13][13], pair<int, int> *jogada_p1, pair<int, int> *jogada_p2, int turno, Player *player1, Player *player2);
        

};

#endif