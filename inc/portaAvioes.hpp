#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP
#include "embarcacao.hpp"

using namespace std;

class PortaAvioes: public Embarcacao{

    public:
        PortaAvioes();
        ~PortaAvioes();

        bool destroi_pa();
};


#endif