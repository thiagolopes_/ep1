#ifndef MENU_HPP
#define MENU_HPP
#include "player.hpp"

using namespace std;

class Menu{
    public:
        Menu();
        ~Menu();

        void mostra_menu();
        void cadastra_player(Player *player1, Player *player2);
        void mostra_mapa(char tabuleirop1[13][13], char tabuleirop2[13][13], Player *player1, Player *player2);
        void mostra_ENDGAME(Player *player1, Player *player2, int turno);

};

#endif