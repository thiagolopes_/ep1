# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste ma criação de um jogo em C++ similar ao game Battleship (ou batalha naval), aplicando conceitos básicos de Orientação a Objetos.
O jogo funciona de maneira bem simples. É um jogo feito para duas pessoas, sendo que a primeira que destruir todas as embarcações de seu rival se sagra vencedor. Para destruir as embarcações, deve-se acertar as coordenadas onde elas estão localizadas, sendo que há diferentes tipos de barcos, e cada um possui uma característica, sendo eles: 

 * **Canoa**: A Canoa ocupa apenas um espaço no tabuleiro, e não possui nenhuma habilidade especial. É a embarcação mais básica do programa;

 * **Submarino**: O Submarino ocupa dois espaços no tabuleiro, e sua habilidade especial é sobreviver ao ataque que sofrer sem tomar danos;

 * **Porta-Aviões**: O Porta-Avião é a maior embarcação, ocupando quatro espaços no tabuleiro. Além disso, sua habilidade é ter 50% de chance de conseguir abater um míssil que vier em sua direção.

## Instruções de Jogo

Ao inicializar o programa, será perguntado primeiramente, o nome do primeiro jogador, e em seguida, o nome do segundo jogador.

Após a escolha dos nomes, os mapas serão gerados, assim como todas as posições dos barcos. Nessa hora, os jogadores poderão ver seu tabuleiro juntamente à seu nome e vida.

Logo em seguida, será pedida uma posição ao Player 1, que sempre fará a primeira jogada. Após à jogada, o Player 2 será solicitado, e assim sucessivamente.

Se um dos Players acertar uma Canoa, um **[C]** será mostrado no tabuleiro do adversário, na posição em que ele se encontra. 
Se um submarino for acertado uma vez, um **[?]** será mostrado no tabuleiro do adversário. Caso haja um segundo acerto, um **[S]** aparecerá no mesmo local.
No caso do Porta-Aviões, se acertado, há uma chance de 50% ser destruído. Caso isso não ocorra, um **[?]** será mostrado. Se sim, um **[P]** aparecerá.
Se nada for acertado, um **[X]** será posicionado na posição referente.

Quando uma Embarcação é destruída, o adversário perde uma **Vida**.

Caso um dos jogadores já tenha colocado uma determinada posição (exclui-se os casos onde a embarcação foi atingida, mas não destruída), este poderá repetir sua jogada, até que confirme uma posição antes não alvejada. Caso uma posição inexistente seja escolhida, o mesmo deverá repetir a jogada.

O jogo acaba quando a **Vida** de um dos players seja 0, mostrando o menu do vencedor.

## Instruções para Inicialização

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações

* O projeto foi desenvolvido em Linux, e por isso, **pode** ser necessária a utilização do mesmo para que o programa seja executado.
* Para testar outros mapas, é **NECESSÁRIO** que o arquivo seja o mesmo. Para que funcione, copie as informações do outro mapa e cole neste. (Não faço ideia do motivo)
* Agradeço ao professor e aos monitores pela proposta do projeto. 

