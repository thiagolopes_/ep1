#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <utility>
#include <unistd.h>
#include "../inc/mapa.hpp"
#include "../inc/menu.hpp"
#include "../inc/player.hpp"
#include "../inc/submarino.hpp"
#include "../inc/jogo.hpp"

using namespace std;

int main(){

    Menu *menu = new Menu();
    Mapa mapa;
    Jogo jogo;
    Player *player1 = new Player();
    Player *player2 = new Player();
    

    menu->mostra_menu();
    menu->cadastra_player(player1, player2);

    vector<pair <int, pair <int, string> > > vector1;
	vector<pair <int, pair <int, string> > > vector2;
    char tabuleirop1[13][13]; char tabuleirop2[13][13];
    pair<int, int> jogada_p1;
    pair<int, int> jogada_p2; 
    int turno = 0;

    mapa.cria_mapa(tabuleirop1, tabuleirop2);
    mapa.le_mapa(&vector1, &vector2);
    
    while (true){
        system("clear");
        menu->mostra_mapa(tabuleirop1, tabuleirop2, player1, player2);
        cout << "Turno: " << turno+1 << endl;
        jogo.recebe_jogada(&jogada_p1, &jogada_p2, turno);
        jogo.valida_jogada(&vector1, &vector2, tabuleirop1, tabuleirop2, &jogada_p1, &jogada_p2, turno, player1, player2);
        sleep(2);
        if(player1->get_vida() == 0 || player2->get_vida() == 0){
            break;
        }
        turno++;
        
    }
    menu->mostra_ENDGAME(player1, player2, turno);

    cout << endl;

    delete player1;
    delete player2;
    delete menu;
    

    return 0;
}
