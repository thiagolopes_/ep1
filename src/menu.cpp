#include "../inc/player.hpp"
#include "../inc/menu.hpp"
#include <iostream>
#include <string>
#include <unistd.h>

using namespace std;

Menu::Menu(){

}
Menu::~Menu(){

}

void Menu::mostra_menu(){
    cout << "WELCOME TO: " << endl;
    cout << "╔═══════════╗	╔═══════════╗  ╠════════╦════════╣  ║             ╔═══════════╗                   " << endl;
    cout << "║           ║	║	    ║	        ║	    ║		  ║                               " << endl;
    cout << "║           ║	║           ║    	║    	    ║		  ║                               " << endl;
    cout << "║           ║	║	    ║	        ║	    ║		  ║                               " << endl;
    cout << "║           ║	║	    ║	        ║	    ║		  ║                               " << endl;
    cout << "╠═══════════╩╗	╠═══════════╣           ║	    ║		  ╠═════════                      " << endl;
    cout << "║	     ║	║	    ║	        ║	    ║		  ║                               " << endl;
    cout << "║            ║	║           ║	        ║           ║		  ║                               " << endl;
    cout << "║	     ║	║           ║	        ║           ║		  ║                               " << endl;
    cout << "║	     ║	║           ║    	║	    ║	          ║                               " << endl;
    cout << "║	     ║  ║           ║           ║	    ║	          ║                               " << endl;
    cout << "╚════════════╝  ║		                    ╚═══════════  ╚═══════════╝                   " << endl;
    cout << "	        ║	                                                                          " << endl;
    cout << "      		║                                                                                 " << endl;
    cout << "                                                                     ║                                    " << endl;
    cout << "			                ╔═════════════╗              ║    ═══════╦═══════  ╔═════════════╗" << endl;
    cout << "			                ║                ║ 	     ║           ║         ║             ║" << endl;
    cout << "			                ║                ║           ║           ║         ║             ║" << endl;
    cout << "			                ║                ║           ║           ║         ║             ║" << endl;
    cout << "			                ║                ║           ║           ║         ║             ║" << endl;
    cout << "			                ║                ║           ║           ║         ║             ║" << endl;
    cout << "			                ╚═════════════╗  ╠═══════════╣           ║         ╠═════════════╝" << endl;
    cout << "					              ║  ║           ║           ║         ║              " << endl;
    cout << "					              ║  ║           ║           ║         ║              " << endl;
    cout << "	                    		              ║  ║           ║           ║         ║              " << endl;
    cout << "				                      ║  ║           ║           ║         ║              " << endl;
    cout << "				                      ║  ║           ║           ║         ║              " << endl;
    cout << "                                        ╚═════════════╝  ║           ║    ═══════╩═══════  ║              " << endl;
    cout << "                                                         ║                                                " << endl;
    cout << "                                                         ║                                                " << endl;
    cout << "                                                                                  Tenha uma ótima partida!" << endl;
    cout << "                                                                     Made and ''designed'' by Thiago Lopes" << endl;
    sleep(3);
    system("clear");

}

void Menu::cadastra_player(Player *player1, Player *player2){
    string nome;

    cout << "\n--Player 1--" << endl;
    
    cout << "Nome: ";
    cin >> nome;
    cout << endl;

    system("clear");

    player1->Player::set_nome(nome);

    cout << "\n--Player 2--" << endl;
    
    cout << "Nome: ";
    cin >> nome;
    cout << endl;

    player2->Player::set_nome(nome);
    system("clear");

    cout << "Boa sorte " << player1->get_nome() << " e " << player2->get_nome() << ".\n\nE que vença o melhor!" << endl;
    sleep(2);
    system("clear");
}
void Menu::mostra_mapa(char tabuleirop1[13][13], char tabuleirop2[13][13], Player *player1, Player *player2){
    
        int i = 0, j = 0, ii = 0;
        
        string nome_p1 = player1->get_nome();
        int cont = nome_p1.length();
        string conta_espaco_p = "    ";
        string conta_espaco_v = "    ";
        cout << "  ";
        for (i = 0; i < 13; i++){
            printf(" %.2d", i);
        }
        string conta_espaco = "";
        int contador;
        if(cont == 2){
            contador = cont + 14;
            for(i = 0; i < contador; i++){
                conta_espaco.push_back(' ');
            }
        }else{
            contador = cont + 13;
            for(i = 0; i < contador; i++){
            conta_espaco.push_back(' ');
            }    
        }
        cout << conta_espaco;
        for (i = 0; i < 13; i++){
            printf(" %.2d", i);
        }
        cout << endl;
        

        for(i = 0; i < cont+8; i++){
            conta_espaco_p.push_back(' ');
        }
        for(i = 0; i < cont-1; i++){
            conta_espaco_v.push_back(' ');
        }

        nome_p1.push_back(' ');
        nome_p1.push_back(' ');
        for(i = 0; i < 13; i++){
		    printf("%.2d", i);
            for(j = 0; j < 13; j++){
                cout << "[" << tabuleirop1[i][j] << "]";
                if (i == 6 && j == 12){
                    cout << "Player: " << nome_p1 << "  ";
                }    
                if (i == 7 && j == 12){
                    printf("Vidas: %.2d", player1->get_vida());
                }
                
            } 
            if (i != 6 && i != 7){   
                cout << conta_espaco_p;
            } 
            if (i == 7){
                cout << conta_espaco_v;
            }
            
            printf("%.2d", i);
            for(ii = 0; ii < 13; ii++){
                cout << "[" << tabuleirop2[i][ii] << "]";
                if(i == 6 && ii == 12){
                    cout << "Player: " << player2->get_nome();
                }
                if(i == 7 && ii == 12){
                    printf("Vidas: %.2d", player2->get_vida());
                }
            }
            cout << endl;   
	    }
}
void Menu::mostra_ENDGAME(Player *player1, Player *player2, int turno){
    system("clear");

    if (turno % 2 == 0){
        cout << "Parabéns " << player1->get_nome() << ", YOU ARE IN THE END GAME NOW." << endl;
        if(player1->get_vida() == 1){
            cout << "Te sobrou APENAS " << player1->get_vida() << " vida." << endl;
        }else{
            cout << "Te sobraram " << player1->get_vida() << " vidas." << endl;
        }
        cout << "Levou " << turno << " Turnos para que a partida fosse encerrada" << endl;
    }else{
        cout << "Parabéns " << player2->get_nome() << ", YOU ARE IN THE END GAME NOW." << endl;
        if(player2->get_vida() == 1){
            cout << "Te sobrou APENAS " << player2->get_vida() << " vida." << endl;
        }else{
            cout << "Te sobraram " << player2->get_vida() << " vidas." << endl;
        }
        cout << "Levou " << turno << " Turnos para que a partida fosse encerrada" << endl;
    }
}



