#include <iostream>
#include <string>
#include "../inc/player.hpp"

using namespace std;


Player::Player(){
    set_nome("");
    set_vida(22);
}       

Player::~Player(){}

string Player::get_nome(){
    return nome;
}

void Player::set_nome(string nome){
    this -> nome = nome;
}

int Player::get_vida(){
	return vida;
}

void Player::set_vida(int vida){
	this -> vida = vida;
}

void Player::perde_vida(int vida){
    this->vida += vida;
}