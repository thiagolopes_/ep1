#include "../inc/jogo.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaAvioes.hpp"
#include "../inc/player.hpp"
#include <iostream>
#include <vector>
#include <utility>

using namespace std;

Jogo::Jogo(){

}
Jogo::~Jogo(){

}

void Jogo::recebe_jogada(pair<int, int> *jogada_p1, pair<int, int> *jogada_p2, int turno){
    
    if (turno%2 == 0){
        cout << endl << "Player 1, Faça sua jogada (Ex: 5 10): " << endl;
        cin >> jogada_p1->first >> jogada_p1->second;
        while (jogada_p1->first < 0 || jogada_p1->first > 12 || jogada_p1->second < 0 || jogada_p1->second > 12){
            cout << "Coordenadas Inválidas, Tente novamente (0 a 12): " << endl;
            cin >> jogada_p1->first >> jogada_p1->second;
        }
    }else{
        cout << endl << "Player 2, Faça sua jogada (Ex: 0 2): " << endl;
        cin >> jogada_p2->first >> jogada_p2->second;
        while (jogada_p2->first < 0 || jogada_p2->first > 12 || jogada_p2->second < 0 || jogada_p2->second > 12){
            cout << "Coordenadas Inválidas, Tente novamente (0 a 12): " << endl;
            cin >> jogada_p2->first >> jogada_p2->second;
        }
    }

}

Canoa canoa;
Submarino submarino;
PortaAvioes p_a;

void Jogo::valida_jogada(vector<pair <int, pair <int, string> > > *vector1, vector<pair <int, pair <int, string> > > *vector2, char tabuleirop1[13][13], char tabuleirop2[13][13], pair<int, int> *jogada_p1, pair<int, int> *jogada_p2, int turno, Player *player1, Player *player2){
    bool verifica_jogada;
    if (turno % 2 == 0){
        while (tabuleirop2[jogada_p1->first][jogada_p1->second] == 'X' || tabuleirop2[jogada_p1->first][jogada_p1->second] == canoa.get_id() || tabuleirop2[jogada_p1->first][jogada_p1->second] == submarino.get_id() || tabuleirop2[jogada_p1->first][jogada_p1->second] == p_a.get_id()){
            cout << endl << "Você já jogou nessa posição, tente novamente." << endl;
            recebe_jogada(jogada_p1, jogada_p2, turno);
        }
        for (auto i:*vector2){
            if (jogada_p1->first == i.first && jogada_p1->second == i.second.first){
                if(i.second.second == "canoa"){
                    if(canoa.destroi_canoa()){
                        tabuleirop2[jogada_p1->first][jogada_p1->second] = canoa.get_id();
                        cout << "Canoa abatida com sucesso!" << endl;
                        player2->perde_vida(-1);    
                        verifica_jogada = true;
                    }
                } 
                if(i.second.second == "submarino"){
                    if(submarino.destroi_submarino(*jogada_p1, *jogada_p2, turno)){
                        tabuleirop2[jogada_p1->first][jogada_p1->second] = submarino.get_id();
                        cout << "Submarino abatido com sucesso!" << endl;
                        player2->perde_vida(-1);
                        verifica_jogada = true;
                    }else{
                        tabuleirop2[jogada_p1->first][jogada_p1->second] = '?';
                        cout << "Você acertou algo que está no fundo do mar, mas nada foi destruído" << endl;
                        verifica_jogada = true;
                    }
                }
                if(i.second.second == "porta-avioes"){
                    if(p_a.destroi_pa()){
                        tabuleirop2[jogada_p1->first][jogada_p1->second] = p_a.get_id();
                        cout << "Porta-Aviões abatido com sucesso!" << endl;
                        player2->perde_vida(-1);
                        verifica_jogada = true;
                    }else{
                        tabuleirop2[jogada_p1->first][jogada_p1->second] = '?';
                        cout << "Uma embarcação de grande porte foi alvejada, mas o míssil foi abatido!" << endl;
                        verifica_jogada = true;
                    }
                }
            }
             
        }
        if(!verifica_jogada){
            tabuleirop2[jogada_p1->first][jogada_p1->second] = 'X';
            cout << "Você errou seu disparo!" << endl;
        }
    }else{  while (tabuleirop1[jogada_p2->first][jogada_p2->second] == 'X' || tabuleirop1[jogada_p2->first][jogada_p2->second] == canoa.get_id() || tabuleirop1[jogada_p2->first][jogada_p2->second] == submarino.get_id() || tabuleirop1[jogada_p2->first][jogada_p2->second] == p_a.get_id()){
                cout << endl << "Você já jogou nessa posição, tente novamente." << endl;
                recebe_jogada(jogada_p1, jogada_p2, turno);
            }
            for (auto i:*vector1){
                if (jogada_p2->first == i.first && jogada_p2->second == i.second.first){
                    if(i.second.second == "canoa"){
                        if(canoa.destroi_canoa()){
                            tabuleirop1[jogada_p2->first][jogada_p2->second] = canoa.get_id();
                            cout << "Canoa abatida com sucesso!" << endl;
                            player1->perde_vida(-1);    
                            verifica_jogada = true;
                        }
                    } 
                    if(i.second.second == "submarino"){
                        if(submarino.destroi_submarino(*jogada_p1, *jogada_p2, turno)){
                            tabuleirop1[jogada_p2->first][jogada_p2->second] = submarino.get_id();
                            cout << "Submarino abatido com sucesso!" << endl;
                            player1->perde_vida(-1);
                            verifica_jogada = true;
                        }else{
                            tabuleirop1[jogada_p2->first][jogada_p2->second] = '?';
                            cout << "Você acertou algo que está no fundo do mar, mas nada foi destruído" << endl;
                            verifica_jogada = true;
                        }
                    }
                    if(i.second.second == "porta-avioes"){
                        if(p_a.destroi_pa()){
                            tabuleirop1[jogada_p2->first][jogada_p2->second] = p_a.get_id();
                            cout << "Porta-Aviões abatido com sucesso!" << endl;
                            player1->perde_vida(-1);
                            verifica_jogada = true;
                        }else{
                            tabuleirop1[jogada_p2->first][jogada_p2->second] = '?';
                            cout << "Uma embarcação de grande porte foi alvejada, mas o míssil foi abatido!" << endl;
                            verifica_jogada = true;
                        }
                    }
                }
                
            }
            if(!verifica_jogada){
                tabuleirop1[jogada_p2->first][jogada_p2->second] = 'X';
                cout << "Você errou seu disparo!" << endl;
            }
    }    
}
 

