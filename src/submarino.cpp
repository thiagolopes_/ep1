#include "../inc/submarino.hpp"
#include <utility>
#include <map>

using namespace std;

map <pair <int, int>, int> map_p1;
map <pair <int, int>, int> map_p2;
//um map para cada player para não haver conflito na hora de destruir a embarcação.

Submarino::Submarino(){
    set_tipo("submarino");
    set_id('S');
    set_tamanho(2);
}
Submarino::~Submarino(){

}

bool Submarino::destroi_submarino(pair <int, int> jogada_p1, pair<int, int> jogada_p2, int turno){
    if(turno % 2 == 0){
        map_p1[jogada_p1]++;
        if(map_p1[jogada_p1] == 2){
            return true;
        }
    }else{
        map_p2[jogada_p2]++;
        if (map_p2[jogada_p2] == 2){
            return true;
        }
    }
    return false;
}
